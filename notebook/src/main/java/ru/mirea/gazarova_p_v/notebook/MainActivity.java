package ru.mirea.gazarova_p_v.notebook;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import kotlin.text.Charsets;
import ru.mirea.gazarova_p_v.notebook.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    EditText editText1;
    EditText editText2;
    Button button1;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        editText1 = binding.editText1;
        editText2 = binding.editText2;
        button1 = binding.button1;
        button2 = binding.button2;

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                String fileName = String.valueOf(editText1.getText());
                File file = new File(path, fileName + ".txt");
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsoluteFile());
                    OutputStreamWriter output = new OutputStreamWriter(fileOutputStream);
                    String data = String.valueOf(editText2.getText());
                    output.write(data);
                    output.close();
                } catch (IOException e) {
                    Log.w("ExternalStorage", "Error writing " + file, e);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);
                String fileName = String.valueOf(editText1.getText());
                File file = new File(path, fileName + ".txt");
                try {
                    FileInputStream fileInputStream = new FileInputStream(file.getAbsoluteFile());

                    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);

                    List<String> lines = new ArrayList<>();
                    BufferedReader reader = new BufferedReader(inputStreamReader);
                    String line = reader.readLine();
                    while (line != null) {
                        lines.add(line);
                        line = reader.readLine();
                    }
                    editText2.setText(lines.toString());
                    Log.w("ExternalStorage", String.format("Read from file %s successful", lines.toString()));
                } catch (Exception e) {
                    Log.w("ExternalStorage", String.format("Read from file %s failed", e.getMessage()));
                }
            }
        });
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}