package ru.mirea.gazarova_p_v.employeedb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SuperheroDao {
    @Query("SELECT * FROM superhero")
    List<Superhero> getAll();
    @Query("SELECT * FROM superhero WHERE id = :id")
    Superhero getById(long id);
    @Insert
    void insert(Superhero employee);
    @Update
    void update(Superhero employee);
    @Delete
    void delete(Superhero employee);
}
