package ru.mirea.gazarova_p_v.employeedb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase db = App.getInstance().getDatabase();
        SuperheroDao superheroDao = db.superheroDao();;

        Superhero superhero = new Superhero();
        superhero.fullName = "Thor Odinson";
        superhero.age = 35;
        superhero.ability = "Superhuman strength";
        superhero.equipment = "Hammer Mjolnir";

        superheroDao.insert(superhero);

        List<Superhero> superheroes = superheroDao.getAll();

        superhero = superheroDao.getById(1);
        superhero.age = 36;
        superheroDao.update(superhero);

        Log.d("SUPERHERO", superhero.fullName + " " + superhero.age + " " + superhero.ability + " " + superhero.equipment);

    }
}