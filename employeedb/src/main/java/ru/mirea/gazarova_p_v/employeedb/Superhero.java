package ru.mirea.gazarova_p_v.employeedb;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Superhero {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String fullName;
    public int age;
    public String ability;
    public String equipment;
}
