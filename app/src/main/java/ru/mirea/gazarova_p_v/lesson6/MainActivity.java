package ru.mirea.gazarova_p_v.lesson6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.mirea.gazarova_p_v.lesson6.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    EditText editText1;
    EditText editText2;
    EditText editText3;

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        editText1 = binding.editText1;
        editText2 = binding.editText2;
        editText3 = binding.editText3;
        button = binding.button;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("mirea_settings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("GROUP", "BSBO-01-20");
                editor.putInt("NUMBER", 9);
                editor.putString("FAVOURITE_FILM", "Harry Potter");
                editor.apply();
                editText1.setText(sharedPref.getString("GROUP", ""));
                editText2.setText(String.valueOf(sharedPref.getInt("NUMBER", -1)));
                editText3.setText(sharedPref.getString("FAVOURITE_FILM", ""));
            }
        });
    }
}